image: gradle:8.0-jdk11

before_script:
  - GRADLE_USER_HOME="$(pwd)/.gradle"
  - export GRADLE_USER_HOME

variables:
  SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
  GIT_DEPTH: "0"

stages:
- build
- test
- deploy

build:
  stage: build
  rules:
    - if: $CI_PIPELINE_SOURCE == "push" && ($CI_COMMIT_REF_PROTECTED || $CI_COMMIT_TAG)
      when: always
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: always
  script:
    - gradle --configure-on-demand --build-cache :plantuml-gradle-plugin:publishToMavenLocal
    - gradle --build-cache assemble
  cache:
    key: "$CI_COMMIT_REF_NAME"
    policy: push
    paths:
      - '**/build'
      - .gradle

junit:
  stage: test
  needs: ["build"]
  rules:
    - if: $CI_PIPELINE_SOURCE == "push" && ($CI_COMMIT_REF_PROTECTED || $CI_COMMIT_TAG)
      when: always
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: always
  script:
    - apt update && apt install -y graphviz
    - gradle --configure-on-demand :plantuml-gradle-plugin:check
  artifacts:
    paths:
      - '**/build/reports/**'
      - '**/build/tmp/test/examples/**'
    expire_in: 1 day
    when: always
  cache:
    key: "$CI_COMMIT_REF_NAME"
    policy: pull-push
    paths:
      - '**/build'
      - .gradle

sonarcloud:
  image: gradle:8.0-jdk17
  stage: test
  needs: ["build", "junit"]
  rules:
    - if: $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH && $CI_COMMIT_REF_PROTECTED
      when: always
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: always
  script:
    - gradle --configure-on-demand :plantuml-gradle-plugin:sonar
  cache:
    - key: "$CI_COMMIT_REF_NAME"
      policy: pull
      paths:
        - '**/build'
        - .gradle
    - key: "$CI_JOB_NAME"
      policy: pull-push
      paths:
        - .sonar/cache

deploy:
  stage: deploy
  needs: ["build", "junit"]
  rules:
    - if: $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_TAG
      when: always
  script:
    - gradle --configure-on-demand :plantuml-gradle-plugin:publishPlugins -Pgradle.publish.key=$GRADLE_PUBLISH_KEY -Pgradle.publish.secret=$GRADLE_PUBLISH_SECRET -Pversion=$CI_COMMIT_TAG
  cache:
    key: "$CI_COMMIT_REF_NAME"
    policy: pull
    paths:
      - '**/build'
      - .gradle
