/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.plunts.gradle.plantuml.plugin;

import groovy.lang.Closure;
import io.github.classgraph.ClassInfo;
import io.github.classgraph.FieldInfo;
import io.github.classgraph.MethodInfo;
import io.gitlab.plunts.gradle.plantuml.plugin.matcher.BaselineMatcher;
import io.gitlab.plunts.gradle.plantuml.plugin.matcher.ClassMatcher;
import io.gitlab.plunts.gradle.plantuml.plugin.matcher.FieldMatcher;
import io.gitlab.plunts.gradle.plantuml.plugin.matcher.IncludeExclude;
import io.gitlab.plunts.gradle.plantuml.plugin.matcher.InterfaceMatcher;
import io.gitlab.plunts.gradle.plantuml.plugin.matcher.MethodMatcher;
import io.gitlab.plunts.gradle.plantuml.plugin.matcher.PackageMatcher;
import io.gitlab.plunts.gradle.plantuml.plugin.matcher.ReferencedClassMatcher;
import io.gitlab.plunts.gradle.plantuml.plugin.matcher.SubclassMatcher;
import io.gitlab.plunts.gradle.plantuml.plugin.matcher.SuperclassMatcher;
import io.gitlab.plunts.gradle.plantuml.plugin.output.AbstractDiagramWriter;
import io.gitlab.plunts.gradle.plantuml.plugin.output.DefaultDiagramWriter;
import io.gitlab.plunts.gradle.plantuml.plugin.output.InsertingDiagramWriter;
import io.gitlab.plunts.gradle.plantuml.plugin.output.RenderingDiagramWriter;
import io.gitlab.plunts.gradle.plantuml.plugin.relation.AssociativeRelation;
import io.gitlab.plunts.gradle.plantuml.plugin.relation.ExtensionRelation;
import io.gitlab.plunts.gradle.plantuml.plugin.relation.RelationOverride;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.gradle.api.Action;
import org.gradle.api.InvalidUserDataException;
import org.gradle.api.file.FileCollection;
import org.gradle.api.tasks.Optional;

/**
 * Gradle DSL extension to configure class diagrams for the project.
 */
@Getter
@Setter
public class ClassDiagramsExtension {

  private static final String LEFT = "left";
  private static final String RIGHT = "right";
  private static final String TOP = "top";
  private static final String BOTTOM = "bottom";
  private final ClassDiagramDefaults defaults = new ClassDiagramDefaults(null);
  private final List<ClassDiagram> diagrams = new ArrayList<>();
  private String plantumlServer = "https://www.plantuml.com/plantuml";
  @Optional
  private FileCollection renderClasspath;

  void inheritFrom(ClassDiagramsExtension parentExt) {
    this.plantumlServer = parentExt.plantumlServer;
    this.renderClasspath = parentExt.renderClasspath;
    this.defaults.defaults = parentExt.defaults;
  }

  public void renderClasspath(FileCollection renderClasspath) {
    this.renderClasspath = renderClasspath;
  }

  public void defaults(Action<ClassDiagramDefaults> action) {
    action.execute(defaults);
  }

  public void diagram(Action<ClassDiagram> action) {
    diagram(null, action);
  }

  public void diagram(String name, Action<ClassDiagram> action) {
    ClassDiagram diagram = new ClassDiagram(name, this.defaults);
    action.execute(diagram);
    diagrams.add(diagram);
  }

  public void defaults(Closure<ClassDiagram> closure) {
    closure.setDelegate(defaults);
    closure.call(defaults);
  }

  public void diagram(Closure<ClassDiagram> closure) {
    diagram(null, closure);
  }

  public void diagram(String name, Closure<ClassDiagram> closure) {
    ClassDiagram diagram = new ClassDiagram(name, this.defaults);
    closure.setDelegate(diagram);
    closure.call(diagram);
    diagrams.add(diagram);
  }

  public static class ClassDiagramDefaults implements Serializable {

    private ClassDiagramDefaults defaults;
    private boolean inheritDefaults = true;
    private final IncludeExclude<ClassInfo, BaselineMatcher> baselineIncludeExclude = new IncludeExclude<>();
    private final IncludeExclude<ClassInfo, SuperclassMatcher> superclassIncludeExclude = new IncludeExclude<>();
    private final IncludeExclude<ClassInfo, SubclassMatcher> subclassIncludeExclude = new IncludeExclude<>();
    private final IncludeExclude<ClassInfo, InterfaceMatcher> interfaceIncludeExclude = new IncludeExclude<>();
    private final IncludeExclude<ClassInfo, ReferencedClassMatcher> referencesIncludeExclude = new IncludeExclude<>();
    private final IncludeExclude<FieldInfo, FieldMatcher> fieldIncludeExclude = new IncludeExclude<>();
    private final IncludeExclude<MethodInfo, MethodMatcher> methodIncludeExclude = new IncludeExclude<>();
    private final Collection<RelationOverride> relationOverrides = new ArrayList<>();
    private final Collection<ElementNote<ClassMatcher>> classNotes = new ArrayList<>();
    private final Collection<ElementNote<PackageMatcher>> packageNotes = new ArrayList<>();
    private final Collection<ElementNote<FieldMatcher>> fieldNotes = new ArrayList<>();
    private final Collection<ElementNote<MethodMatcher>> methodNotes = new ArrayList<>();
    @Getter
    private final Style style = new Style();

    ClassDiagramDefaults(ClassDiagramDefaults defaults) {
      this.defaults = defaults;
    }

    public void style(Action<Style> action) {
      action.execute(style);
    }

    public void style(Closure<Style> closure) {
      closure.setDelegate(style);
      closure.call(style);
    }

    public void dontInheritDefaults() {
      this.inheritDefaults = false;
    }

    public void include(BaselineMatcher matcher) {
      baselineIncludeExclude.addInclude(matcher);
    }

    public void include(SuperclassMatcher matcher) {
      superclassIncludeExclude.addInclude(matcher);
    }

    public void include(SubclassMatcher matcher) {
      subclassIncludeExclude.addInclude(matcher);
    }

    public void include(InterfaceMatcher matcher) {
      interfaceIncludeExclude.addInclude(matcher);
    }

    public void include(ReferencedClassMatcher matcher) {
      referencesIncludeExclude.addInclude(matcher);
    }

    public void include(FieldMatcher matcher) {
      fieldIncludeExclude.addInclude(matcher);
    }

    public void include(MethodMatcher matcher) {
      methodIncludeExclude.addInclude(matcher);
    }

    public void exclude(BaselineMatcher matcher) {
      baselineIncludeExclude.addExclude(matcher);
    }

    public void exclude(SuperclassMatcher matcher) {
      superclassIncludeExclude.addExclude(matcher);
    }

    public void exclude(SubclassMatcher matcher) {
      subclassIncludeExclude.addExclude(matcher);
    }

    public void exclude(InterfaceMatcher matcher) {
      interfaceIncludeExclude.addExclude(matcher);
    }

    public void exclude(ReferencedClassMatcher matcher) {
      referencesIncludeExclude.addExclude(matcher);
    }

    public void exclude(FieldMatcher matcher) {
      fieldIncludeExclude.addExclude(matcher);
    }

    public void exclude(MethodMatcher matcher) {
      methodIncludeExclude.addExclude(matcher);
    }

    public ElementNoteBuilder note(String note) {
      return new ElementNoteBuilder(note);
    }

    public ClassMatcher classes() {
      return new ClassMatcher();
    }

    public ClassMatcher classes(String name) {
      return new ClassMatcher().withNameLike(name);
    }

    public PackageMatcher packages() {
      return new PackageMatcher();
    }

    public PackageMatcher packages(String name) {
      return new PackageMatcher().withNameLike(name);
    }

    public SuperclassMatcher superclasses() {
      return new SuperclassMatcher();
    }

    public SubclassMatcher subclasses() {
      return new SubclassMatcher();
    }

    public InterfaceMatcher interfaces() {
      return new InterfaceMatcher();
    }

    public ReferencedClassMatcher referencedClasses() {
      return new ReferencedClassMatcher();
    }

    public FieldMatcher fields() {
      return new FieldMatcher();
    }

    public MethodMatcher methods() {
      return new MethodMatcher();
    }

    /**
     * @deprecated use style-configuration instead
     */
    @Deprecated(since = "2.1.0", forRemoval = true)
    public void hidePackages() {
      style.hidePackages();
    }

    public void override(RelationOverride relationOverride) {
      if (relationOverride.getFrom() == null) {
        throw new InvalidUserDataException("from not specified");
      }
      if (relationOverride.getTo() == null) {
        throw new InvalidUserDataException("from not specified");
      }
      relationOverrides.add(relationOverride);
    }

    public RelationOverride association() {
      return new RelationOverride(AssociativeRelation.class);
    }

    public RelationOverride extension() {
      return new RelationOverride(ExtensionRelation.class);
    }

    public IncludeExclude<ClassInfo, BaselineMatcher> getBaselineIncludeExclude() {
      IncludeExclude<ClassInfo, BaselineMatcher> copy = new IncludeExclude<>(baselineIncludeExclude);
      if (this.inheritDefaults && this.defaults != null) {
        copy.addAll(this.defaults.getBaselineIncludeExclude());
      }
      return copy;
    }

    public IncludeExclude<ClassInfo, SuperclassMatcher> getSuperclassIncludeExclude() {
      IncludeExclude<ClassInfo, SuperclassMatcher> copy = new IncludeExclude<>(superclassIncludeExclude);
      if (this.inheritDefaults && this.defaults != null) {
        copy.addAll(this.defaults.getSuperclassIncludeExclude());
      }
      return copy;
    }

    public IncludeExclude<ClassInfo, SubclassMatcher> getSubclassIncludeExclude() {
      IncludeExclude<ClassInfo, SubclassMatcher> copy = new IncludeExclude<>(subclassIncludeExclude);
      if (this.inheritDefaults && this.defaults != null) {
        copy.addAll(this.defaults.getSubclassIncludeExclude());
      }
      return copy;
    }

    public IncludeExclude<ClassInfo, InterfaceMatcher> getInterfaceIncludeExclude() {
      IncludeExclude<ClassInfo, InterfaceMatcher> copy = new IncludeExclude<>(interfaceIncludeExclude);
      if (this.inheritDefaults && this.defaults != null) {
        copy.addAll(this.defaults.getInterfaceIncludeExclude());
      }
      return copy;
    }

    public IncludeExclude<ClassInfo, ReferencedClassMatcher> getReferencesIncludeExclude() {
      IncludeExclude<ClassInfo, ReferencedClassMatcher> copy = new IncludeExclude<>(referencesIncludeExclude);
      if (this.inheritDefaults && this.defaults != null) {
        copy.addAll(this.defaults.getReferencesIncludeExclude());
      }
      return copy;
    }

    public IncludeExclude<FieldInfo, FieldMatcher> getFieldIncludeExclude() {
      IncludeExclude<FieldInfo, FieldMatcher> copy = new IncludeExclude<>(fieldIncludeExclude);
      if (this.inheritDefaults && this.defaults != null) {
        copy.addAll(this.defaults.getFieldIncludeExclude());
      } else {
        copy.addInclude((FieldMatcher) fields().thatArePublic());
      }
      return copy;
    }

    public IncludeExclude<MethodInfo, MethodMatcher> getMethodIncludeExclude() {
      IncludeExclude<MethodInfo, MethodMatcher> copy = new IncludeExclude<>(methodIncludeExclude);
      if (this.inheritDefaults && this.defaults != null) {
        copy.addAll(this.defaults.getMethodIncludeExclude());
      } else {
        copy.addInclude(((MethodMatcher) methods().thatArePublic()).andNotBuiltin());
      }
      return copy;
    }

    public List<ElementNote<ClassMatcher>> getClassNotes() {
      List<ElementNote<ClassMatcher>> copy = new ArrayList<>();
      if (this.inheritDefaults && this.defaults != null) {
        copy.addAll(this.defaults.getClassNotes());
      }
      copy.addAll(this.classNotes);
      return copy;
    }

    public List<ElementNote<PackageMatcher>> getPackageNotes() {
      List<ElementNote<PackageMatcher>> copy = new ArrayList<>();
      if (this.inheritDefaults && this.defaults != null) {
        copy.addAll(this.defaults.getPackageNotes());
      }
      copy.addAll(this.packageNotes);
      return copy;
    }

    public List<ElementNote<FieldMatcher>> getFieldNotes() {
      List<ElementNote<FieldMatcher>> copy = new ArrayList<>();
      if (this.inheritDefaults && this.defaults != null) {
        copy.addAll(this.defaults.getFieldNotes());
      }
      copy.addAll(this.fieldNotes);
      return copy;
    }

    public List<ElementNote<MethodMatcher>> getMethodNotes() {
      List<ElementNote<MethodMatcher>> copy = new ArrayList<>();
      if (this.inheritDefaults && this.defaults != null) {
        copy.addAll(this.defaults.getMethodNotes());
      }
      copy.addAll(this.methodNotes);
      return copy;
    }

    public Collection<RelationOverride> getRelationOverrides() {
      Collection<RelationOverride> copy = new ArrayList<>(relationOverrides);
      if (this.inheritDefaults && this.defaults != null) {
        copy.addAll(this.defaults.getRelationOverrides());
      }
      return copy;
    }

    public class Style implements Serializable {

      private List<String> includes = new ArrayList<>();
      private List<String> defines = new ArrayList<>();
      private Boolean useIntermediatePackages = null;
      private Boolean showPackages = null;
      private String theme;
      private Map<String, String> skinparams = new LinkedHashMap<>();
      private Map<String, Boolean> show = new LinkedHashMap<>();
      private List<ElementStyle<PackageMatcher>> packageStyles = new ArrayList<>();
      private List<ElementStyle<ClassMatcher>> classStyles = new ArrayList<>();

      public void include(String file) {
        includes.add(file);
      }

      public void useIntermediatePackages() {
        useIntermediatePackages = Boolean.TRUE;
      }

      public void define(String def) {
        defines.add(def);
      }

      public void define(String def1, String def2) {
        define(def1 + " " + def2);
      }

      public void hidePackages() {
        showPackages = Boolean.FALSE;
      }

      public void theme(String theme) {
        this.theme = theme;
      }

      public void skinparam(String name, Object value) {
        skinparams.put(name, Objects.requireNonNull(value, "value must not be null").toString());
      }

      public void hide(String what) {
        show.put(what, Boolean.FALSE);
      }

      public void show(String what) {
        show.put(what, Boolean.TRUE);
      }

      public ElementStyleBuilder addStyle(String style) {
        return new ElementStyleBuilder(style);
      }

      public List<String> getIncludes() {
        List<String> copy = new ArrayList<>();
        if (inheritDefaults && defaults != null) {
          copy.addAll(defaults.style.getIncludes());
        }
        copy.addAll(includes);
        return copy;
      }

      public List<String> getDefines() {
        List<String> copy = new ArrayList<>();
        if (inheritDefaults && defaults != null) {
          copy.addAll(defaults.style.getDefines());
        }
        copy.addAll(defines);
        return copy;
      }

      public boolean isUseIntermediatePackages() {
        if (this.useIntermediatePackages != null) {
          return this.useIntermediatePackages;
        } else if (inheritDefaults && defaults != null) {
          return defaults.style.isUseIntermediatePackages();
        } else {
          return false;
        }
      }

      boolean isShowPackages() {
        if (this.showPackages != null) {
          return this.showPackages;
        } else if (inheritDefaults && defaults != null) {
          return defaults.style.isShowPackages();
        } else {
          return true;
        }
      }

      public String getTheme() {
        if (this.theme != null) {
          return this.theme;
        } else if (inheritDefaults && defaults != null) {
          return defaults.style.getTheme();
        } else {
          return null;
        }
      }

      public Map<String, String> getSkinparams() {
        Map<String, String> copy = new LinkedHashMap<>();
        if (inheritDefaults && defaults != null) {
          copy.putAll(defaults.style.getSkinparams());
        }
        copy.putAll(skinparams);
        return copy;
      }

      public Map<String, Boolean> getShow() {
        Map<String, Boolean> copy = new LinkedHashMap<>();
        if (inheritDefaults && defaults != null) {
          copy.putAll(defaults.style.getShow());
        }
        copy.putAll(show);
        return copy;
      }

      public List<ElementStyle<PackageMatcher>> getPackageStyles() {
        List<ElementStyle<PackageMatcher>> copy = new ArrayList<>();
        copy.addAll(packageStyles);
        if (inheritDefaults && defaults != null) {
          copy.addAll(defaults.style.getPackageStyles());
        }
        return copy;
      }

      public List<ElementStyle<ClassMatcher>> getClassStyles() {
        List<ElementStyle<ClassMatcher>> copy = new ArrayList<>();
        copy.addAll(classStyles);
        if (inheritDefaults && defaults != null) {
          copy.addAll(defaults.style.getClassStyles());
        }
        return copy;
      }

      @AllArgsConstructor
      public class ElementStyleBuilder {

        private final String style;

        public void to(PackageMatcher matcher) {
          packageStyles.add(new ElementStyle<>(matcher, style));
        }

        public void to(ClassMatcher matcher) {
          classStyles.add(new ElementStyle<>(matcher, style));
        }

      }

    }

    @Data
    @AllArgsConstructor
    public static class ElementStyle<M extends BaselineMatcher & Serializable> implements Serializable {

      private final M matcher;
      private final String style;

    }

    @AllArgsConstructor
    public class ElementNoteBuilder {

      private final String note;

      public void leftOf(ClassMatcher matcher) {
        classNotes.add(new ElementNote<>(matcher, LEFT, note));
      }

      public void rightOf(ClassMatcher matcher) {
        classNotes.add(new ElementNote<>(matcher, RIGHT, note));
      }

      public void topOf(ClassMatcher matcher) {
        classNotes.add(new ElementNote<>(matcher, TOP, note));
      }

      public void bottomOf(ClassMatcher matcher) {
        classNotes.add(new ElementNote<>(matcher, BOTTOM, note));
      }

      public void leftOf(PackageMatcher matcher) {
        packageNotes.add(new ElementNote<>(matcher, LEFT, note));
      }

      public void rightOf(PackageMatcher matcher) {
        packageNotes.add(new ElementNote<>(matcher, RIGHT, note));
      }

      public void topOf(PackageMatcher matcher) {
        packageNotes.add(new ElementNote<>(matcher, TOP, note));
      }

      public void bottomOf(PackageMatcher matcher) {
        packageNotes.add(new ElementNote<>(matcher, BOTTOM, note));
      }

      public void leftOf(FieldMatcher matcher) {
        fieldNotes.add(new ElementNote<>(matcher, LEFT, note));
      }

      public void rightOf(FieldMatcher matcher) {
        fieldNotes.add(new ElementNote<>(matcher, RIGHT, note));
      }

      public void leftOf(MethodMatcher matcher) {
        methodNotes.add(new ElementNote<>(matcher, LEFT, note));
      }

      public void rightOf(MethodMatcher matcher) {
        methodNotes.add(new ElementNote<>(matcher, RIGHT, note));
      }

    }

    @Data
    @AllArgsConstructor
    public static class ElementNote<M extends Serializable> implements Serializable {

      private final M matcher;
      private final String position;
      private final String note;

    }

  }

  @Getter
  public static class ClassDiagram extends ClassDiagramDefaults implements Serializable {

    private String name;
    private final List<AbstractDiagramWriter> writers = new ArrayList<>();

    ClassDiagram(String name, ClassDiagramDefaults defaults) {
      super(defaults);
      this.name = name;
    }

    public void name(String name) {
      this.name = name;
    }

    public void writeTo(File outputFile) {
      this.writers.add(new DefaultDiagramWriter(outputFile));
    }

    public void insertInto(File outputFile) {
      this.writers.add(new InsertingDiagramWriter(outputFile));
    }

    public void renderTo(File outputFile) {
      this.writers.add(new RenderingDiagramWriter(outputFile));
    }

  }

}
