package io.gitlab.plunts.gradle.plantuml.plugin.matcher.subjects;

public class ClassWithMethodNamedGetSetAndIs {

    public String get(String someId){
        return "whatever";
    }

    public String is(String someId){
        return "whatever";
    }
    public void set(String someId){
        //noop
    }
}
